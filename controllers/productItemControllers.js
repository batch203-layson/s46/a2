const ProductItem = require("../models/ProductItem");
//const UserAccount = require("../models/UserAccount");
const bcrypt = require("bcrypt");
const auth = require("../auth");




// Create a new product

module.exports.addProductItem = (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    let newProduct = new ProductItem({
        productItemName: req.body.productItemName,
        productItemDescription: req.body.productItemDescription,
        productItemPrice: req.body.productItemPrice,
        productItemStocks: req.body.productItemStocks
    });

    if (userData.userAccountIsAdmin) {
        return newProduct.save()
            .then(product => {
                res.send(true)
            })
            .catch(error => {
                res.send(false);
            });
    }
    else {
        return res.send(false);
    };

};




// Retrieve all product admin only
/*
    Steps:
    1. Retrieve all the product (active/inactive) from the database.
    2. Verify the role of the current user (Admin).



*/
module.exports.getAllProducts = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if (userData.userAccountIsAdmin) {
        return ProductItem.find({}).then(result => res.send(result));
    }
    else {
        // return res.send(false);
        return res.status(401).send("You don't have access to this page!");
    }

}





// Retrieve All Active Product
/*
    Step:
        1. Retrieve all the product from the database with the property "isActive" true.


*/
module.exports.getAllActive = (req, res) => {
    return ProductItem.find({ productItemIsActive: true }).then(result => res.send(result));
}





// Retrieving a specific product

module.exports.getProduct = (req, res) => {


    return ProductItem.findById(req.params.productItemId).then(result => res.send(result));
}

// Update product admin only

module.exports.updateProduct = (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if (userData.userAccountIsAdmin) {
        let updateProduct = {
            productItemName: req.body.productItemName,
            productItemDescription: req.body.productItemDescription,
            productItemPrice: req.body.productItemPrice,
            productItemStocks: req.body.productItemStocks
        }

        // Syntax
        // findByIdAndUpdate(documentID, updatesToBeApplied, {new:true})

        return ProductItem.findByIdAndUpdate(req.params.productItemId, updateProduct, { new: true })
            .then(result => {
                res.send(result);
            })
            .catch(error => {
                res.send(false);
            });
    }
    else {
        // return res.send(false);
        return res.status(401).send("You don't have access to this page!");
    }
}





// Archive a course
// Soft delete happens when a course status (isActive) is set to false.

module.exports.archiveProduct = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let updateIsActiveField = {
        productItemIsActive: req.body.productItemIsActive
    }

    if (userData.userAccountIsAdmin) {
        return ProductItem.findByIdAndUpdate(req.params.productItemId, updateIsActiveField)
            .then(result => {
                
                res.send({ message: `Product archive success` });
            })
            .catch(error => {
                
                res.send({ message: `Product archive success` });
            })
    }
    else {
        // return res.send(false);
        return res.status(401).send("You don't have access to this page!");
    }
}


