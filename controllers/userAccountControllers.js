const UserAccount = require("../models/UserAccount");
const ProductItem = require("../models/ProductItem");
const bcrypt = require("bcrypt");
const auth = require("../auth");




// Check if the email exists
/*
    Steps: 
    1. Use mongoose "find" method to find duplicate emails
    2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (req, res) =>{
    return UserAccount.find({userAccountEmail: req.body.userAccountEmail}).then(result =>{

        // The result of the find() method returns an array of objects.
             // we can use array.length method for checking the current result length
        console.log(result);

        // The user already exists
        if(result.length > 0){
            return res.send(true);
            // return res.send("User already exists!");
        }
        // There are no duplicate found.
        else{
            return  res.send(false);
            // return res.send("No duplicate found!");
        }
    })
    .catch(error => res.send(error));
}


// User Registration




module.exports.registerUserAccount = (req, res) => {

    let newUser = UserAccount({

        userAccountFirstName: req.body.userAccountFirstName,
        userAccountLastName: req.body.userAccountLastName,
        userAccountEmail: req.body.userAccountEmail,
        /* 
        userAccountPassword: req.body.userAccountPassword,
         */
        userAccountPassword: bcrypt.hashSync(req.body.userAccountPassword, 10),
        userAccountMobileNumber: req.body.userAccountMobileNumber

    })

    //console.log(newUser[{userAccountEmail:req.body.userAccountEmail}]);
    return newUser.save()
        .then(UserAccount => {
            res.send(true);
        })
        .catch(error => {
            res.send(error);
        })

}





// User Login Authentication

module.exports.loginUser = (req, res) => {
    return UserAccount.findOne({ userAccountEmail: req.body.userAccountEmail })
        .then(result => {
            // User does not exists
            if (result == null) {
                // return res.send(false);
                return res.send(true);
            }
            // User exists
            else {
                // Syntax: bcrypt.compareSync(data, encrypted)
                const isPasswordCorrect = bcrypt.compareSync(req.body.userAccountPassword, result.userAccountPassword);

                // If the passwords match/result of the above code is true.
                if (isPasswordCorrect) {
                    // Generate an access token
                    // Uses the "createAccessToken" method defined in the "auth.js" file
                    // Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
                    return res.send({ accessToken: auth.createAccessToken(result)});
                }
                else {
                    // return false; // if password do not match
                    return res.send({ message: "Password is incorrect!" });
                }
            }
        })
}

// Route for user details




     //get user details
/* 
module.exports.getUserDetails = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    return UserAccount.findOne(req.params.userData).then(result => res.send(result));
}
 */

module.exports.getProfile = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    //console.log(userData);

    return UserAccount.findById(userData.id).then(result => {
        result.userAccountPassword = "***";
        res.send(result);
    })
}	


/* 
check out


  */


module.exports.checkout = async (req, res) => {

    const userData = auth.decode(req.headers.authorization)


    if (userData.userAccountAdmin) {
        return res.send("Order placed not authorized");
    }
    else {
        let isUserUpdated = await UserAccount.findById(userData.id).then(user => {
            user.orders.push({
                ordersTotalAmount: req.body.ordersTotalAmount,
                ordersProducts: req.body.ordersProducts
            })

            // Save the updated user information in the database
            return user.save()
                .then(result => {
                    //console.log(result);
                    res.send(result);
                })
                .catch(error => {
                    //console.log(error);
                    return false;
                })
        })
        // console.log(isUserUpdated)
        for (let a = 0; a < req.body.ordersProducts.length; a++) {

            let data = {
                userAccountId: userData.id,
                userAccountEmail: userData.userAccountEmail,
                productItemId: req.body.ordersProducts[a].productItemId,
                productItemName: req.body.ordersProducts[a].productItemName,
                productItemQuantity: req.body.ordersProducts[a].productItemQuantity
            }

            let isProductUpdated = await ProductItem.findById(data.id).then(product => {
                product.orders.push({
                    userAccountId: data.userAccountId,
                    userAccountEmail: data.userAccountEmail,
                    userAccountQuantity: req.body.userAccountQuantity
                })

                // Minus the stocks available by quantity from req.body
                product.productItemStocks -= data.productItemQuantity;

                return product.save()
                    .then(result => {
                        //console.log(result);
                        return true;
                    })
                    .catch(error => {
                        //console.log(error);
                        return false;
                    })
            })
        }

        // Condition will check if the both "user" and "product" document have been updated.

        // (isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false)
    }
}




// set user to admin




// Archive a course
// Soft delete happens when a course status (isActive) is set to false.

module.exports.setUserToAdmin = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let updateIsActiveField = {
        userAccountIsAdmin: req.body.userAccountIsAdmin
    }

    if (userData.userAccountIsAdmin) {
        return UserAccount.findByIdAndUpdate(req.params.userAccountId, updateIsActiveField)
            .then(result => {

                res.send({ message: `Set user to admin success` });
            })
            .catch(error => {

                res.send({ message: `Set user to admin success` });
            })
    }
    else {
        // return res.send(false);
        return res.status(401).send("You don't have access to this page!");
    }
}
